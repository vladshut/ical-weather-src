<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** @var $router Router */

use Laravel\Lumen\Routing\Router;

$router->get('/', 'MainController@index');

$router->get('cities/{query}', 'LocationController@search');
$router->get('forecast-ics/{locationId}/{lang}/{tempScale}', ['uses' => 'IcalController@getForecast', 'as' => 'Ical.getForecast']);
$router->get('forecast/{locationId}', 'ForecastController@index');
