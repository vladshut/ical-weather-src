<?php
namespace Deployer;

require 'recipe/laravel.php';

set('repository', 'git@gitlab.com:vladshut/ical-weather-src.git');

host('root@91.235.128.222')
    ->set('deploy_path', '/home/admin/web/ical-weather.com/public_html');
set('http_user', 'www-data');
// Project name
set('application', 'ical-weather');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server 
add('writable_dirs', []);
    
// Tasks

task('build', function () {
    run('cd {{release_path}}/images/php/app && build');
});

task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:vendors',
    'deploy:writable',
    // 'artisan:storage:link',
//    'artisan:view:clear',
//    'artisan:config:cache',
    'artisan:optimize',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
]);

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

//before('deploy:symlink', 'artisan:migrate');

