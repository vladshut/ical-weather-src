const BASE_URL = 'https://ical-weather.com';
let lang = 'en';
let tempScale = 'c';
let id;

jQuery(document).ready(function ($) {
    const $copyBtn = $('#copy-button');

    const $input = $('#search-city-input');
    const $button = $('#search-city-button');

    const $citiesModal = $('#citiesModal');
    const $modalBody = $citiesModal.find('.modal-body');

    const $locationName = $('#location-name');
    const $locationFullName = $('#location-full-name');
    const $location = $('#location');

    // $copyBtn.tooltip();

    // When the copy button is clicked, select the value of the text box, attempt
    // to execute the copy command, and trigger event to update tooltip message
    // to indicate whether the text was successfully copied.
    $copyBtn.bind('click', function(e) {
        const text = $('#copy-input').val();
        copyToClipboard(text, e);
    });

    function setLink() {
        const link = BASE_URL + '/forecast-ics/' + id + '/' + lang + '/' + tempScale;
        $('#webcal-link input').val(link);
    }

    $('#lang-switcher').on('click', '.btn', (e) => {
        const $btn = $(e.currentTarget);
        const $activeButton = $('#lang-switcher .btn-success');

        if ($btn.hasClass('btn-success')) {
            return;
        }

        $btn.removeClass('btn-default');
        $btn.addClass('btn-success');

        $activeButton.removeClass('btn-success');
        $activeButton.addClass('btn-default');

        lang = $btn.attr('data-value');
        setLink();
    });

    $('#scale-switcher').on('click', '.btn', (e) => {
        const $btn = $(e.currentTarget);
        const $activeButton = $('#scale-switcher .btn-success');

        if ($btn.hasClass('btn-success')) {
            return;
        }

        $btn.removeClass('btn-default');
        $btn.addClass('btn-success');

        $activeButton.removeClass('btn-success');
        $activeButton.addClass('btn-default');

        tempScale = $btn.attr('data-value');
        setLink();
    });

    $('#citiesModal .modal-body').on('click', '.location-option', (e) => {
        const $link = $(e.currentTarget);

        id = $link.data('id');
        const name = $link.attr('data-name');
        const fullName = $link.attr('data-fullName');
        $location.show();
        $locationName.text(name);
        $locationFullName.text(fullName);

        $citiesModal.modal('hide');
        $input.val(null);

        setLink();
        $('#webcal-link').show();
        $('#supports').hide();

        // $.ajax({
        //     url: BASE_URL + '/forecast/' + id,
        //     beforeSend: function () {
        //     },
        //     complete: (response) => {
        //         const data = response.responseJSON;
        //         data.forecast.forEach(d => {
        //             d.date = d.date.date;
        //         });
        //         console.log(data);
        //
        //
        //     }
        // });

    });

    $('#search-city-form').submit(function (event) {
        event.preventDefault();
        const buttonText = $button.html();
        const buttonLoadingText = $button.data('loading-text');

        const query = $input.val();

        if (!query) {
            return;
        }

        $.ajax({
            url: BASE_URL + '/cities/' + query,
            beforeSend: function () {
                $button.html(buttonLoadingText);
                $input.attr('disabled', true);
                $button.attr('disabled', true);
            },
        })
            .done(function (cities) {
                if (!cities || !cities.length) {
                    return;
                }

                $modalBody.empty();

                let elements = $();

                for (let i = 0; i < cities.length; i++) {
                    const element = $(`
                <h3 style="text-transform: capitalize; font-size: 20px" class="mb-3 pl-3">
                  <i class="fa fa-map-marker text-success"></i>&nbsp;&nbsp;
                  <a href="#" style="text-decoration: none; font-weight: 400; letter-spacing: 0.5px; color: #509deff7;" class="location-option"
                    data-id="${cities[i]['id']}"
                    data-name="${cities[i]['name']}"
                    data-fullName="${cities[i]['fullName']}">${cities[i]['name']}</a><span class="text-secondary">,</span>&nbsp;
                  <small class="text-muted">${cities[i]['fullName']}</small>
                </h3>`
                    );
                    elements = elements.add(element);
                }

                $modalBody.append(elements);

                $citiesModal.modal();

                $button.html(buttonText);
                $input.attr('disabled', false);
                $button.attr('disabled', false);
            });
    });
});


function copyToClipboard(str, e) {
    const el = document.createElement('textarea');  // Create a <textarea> element
    el.value = str;                                 // Set its value to the string that you want copied
    el.setAttribute('readonly', '');                // Make it readonly to be tamper-proof
    el.style.position = 'absolute';
    el.style.left = '-9999px';                      // Move outside the screen to make it invisible
    document.body.appendChild(el);                  // Append the <textarea> element to the HTML document
    const selected =
        document.getSelection().rangeCount > 0        // Check if there is any content selected previously
            ? document.getSelection().getRangeAt(0)     // Store selection if found
            : false;                                    // Mark as false to know no selection existed before
    el.select();                                    // Select the <textarea> content
    document.execCommand('copy');                   // Copy - only works as a result of a user action (e.g. click events)
    document.body.removeChild(el);                  // Remove the <textarea> element
    if (selected) {                                 // If a selection existed before copying
        document.getSelection().removeAllRanges();    // Unselect everything on the HTML document
        document.getSelection().addRange(selected);   // Restore the original selection
    }

    showTooltip('Copied!', e);
}

function showTooltip(str, e) {
    const delayShow = 100;
    const delayHide = 5000;

    $('<div class="wy-tooltip wy-hide"></div>').text(str).appendTo('body');

    setTimeout(function () {
        $('.wy-tooltip').removeClass('wy-hide').fadeIn('slow');
        placeTooltip(e.pageX, e.pageY);
    }, delayShow);

    setTimeout(function (e) {
        $('.wy-tooltip').addClass('wy-hide').fadeOut('slow');
    }, delayHide);

    $('body').on('mousemove', function (e) {
        placeTooltip(e.pageX, e.pageY);
    });

    function placeTooltip(CursorX, CursorY) {
        var pLeft;
        var pTop;
        var offset = 10;
        var WindowWidth = $(window).width();
        var WindowHeight = $(window).height();
        var toolTip = $('.wy-tooltip');
        var TTWidth = toolTip.width();
        var TTHeight = toolTip.height();
        if (CursorX - offset >= (WindowWidth / 4) * 3) {
            pLeft = CursorX - TTWidth - offset;
        } else {
            pLeft = CursorX + offset;
        }
        if (CursorY - offset >= (WindowHeight / 4) * 3) {
            pTop = CursorY - TTHeight - offset;
        } else {
            pTop = CursorY + offset;
        }
        $('.wy-tooltip').css({top: pTop, left: pLeft})
    }
}
