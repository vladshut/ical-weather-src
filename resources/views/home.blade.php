<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>iCalWeather | Brings weather forecast to calendar</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Roboto:100,300,400,500,700|Philosopher:400,400i,700,700i"
          rel="stylesheet">

    <!-- Bootstrap css -->
    <!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
    <link href="{{asset('lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="{{asset('lib/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('lib/owlcarousel/assets/owl.theme.default.min.css')}}" rel="stylesheet">
    <link href="{{asset('lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('lib/animate/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('lib/modal-video/css/modal-video.min.css')}}" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/clndr.css')}}" rel="stylesheet">

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('favicon/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('favicon/site.webmanifest')}}">

    <!-- =======================================================
      Theme Name: eStartup
      Theme URL: https://bootstrapmade.com/estartup-bootstrap-landing-page-template/
      Author: BootstrapMade.com
      License: https://bootstrapmade.com/license/
    ======================================================= -->
</head>

<body>

<header id="header" class="header header-hide">
    <div class="container">

        <div id="logo" class="pull-left">
            <h1><a href="#body" class="scrollto"><span>iCal</span>Weather</a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="#body"><img src="{{asset('img/logo.png')}}" alt="" title="" /></a>-->
        </div>

        <nav id="nav-menu-container">
            <ul class="nav-menu">
                <li class="menu-active"><a href="#hero">Home</a></li>
                <li><a href="#about-us">About</a></li>
                <li><a href="#features">Features</a></li>
            </ul>
        </nav><!-- #nav-menu-container -->
    </div>
</header><!-- #header -->

<!--==========================
  Hero Section
============================-->
<section id="hero" class="wow fadeIn">
    <div class="hero-container">
        <h1 class="mt-5">Welcome to iCalWeather</h1>
        <h2 class="mb-5">This service allows you integrate weather forecast in your calendar</h2>
        <div class="row w-100 pb-3 mb-5 mt-5">
            <div class="col-md-4 offset-md-4">
                <form id="search-city-form" class="form-inline justify-content-between flex-nowrap">
                    <div class="form-group w-100">
                        <input id="search-city-input" type="text" class="form-control form-control-lg w-100"
                               placeholder="Enter your location" style="padding-right: 150px">
                    </div>
                    <button id="search-city-button"
                            class="btn btn-success btn-lg"
                            style="border-bottom-left-radius: 0; border-top-left-radius: 0; margin-left: -150px;"
                            data-loading-text="<i class='fa fa-spinner fa-spin '></i>"
                    ><i class="fa fa-search"></i>&nbsp;&nbsp;Search
                    </button>
                </form>
            </div>
        </div>
        <h2 class="" id="location" style="display: none">
            <i class="fa fa-map-marker text-success"></i>
            &nbsp;&nbsp;
            <span id="location-name" class="font-weight-bold">Конотоп</span>,
            &nbsp;
            <span id="location-full-name">Конотопський Район, Сумська Область, Україна</span>
        </h2>
        <br>
        <div id="webcal-link" class="row w-100 pb-3 mb-5 mt-5" style="display: none;">

            <div class="col-md-4 offset-md-4">
                <div class="mb-3 w-100 text-left">
                    <div class="btn-group mr-3" role="group" aria-label="..." id="lang-switcher">
                        <button type="button" class="btn btn-success" data-value="en">EN</button>
                        <button type="button" class="btn btn-default" data-value="ua">UA</button>
                    </div>
                    <div class="btn-group" role="group" aria-label="..." id="scale-switcher">
                        <button type="button" class="btn btn-success" data-value="c">°C</button>
                        <button type="button" class="btn btn-default" data-value="f">°F</button>
                    </div>
                </div>
                <form class="form-inline justify-content-between flex-nowrap">
                    <div class="form-group w-100">
                        <input type="text" class="form-control w-100"
                               value="" placeholder="Some path" id="copy-input" style="padding-right: 150px;">
                    </div>

                    <button class="btn btn-default btn text-secondary" title="Copy to clipboard!" type="button" id="copy-button" style="border-bottom-left-radius: 0; border-top-left-radius: 0; margin-left: -150px;">
                        <i class="fa fa-clipboard"></i>
                    </button>
                </form>
            </div>
        </div>

        <div id="mini-clndr" class="cal1">

            <script type="text/template" id="template-calendar">
                <div class="clndr-controls top">
                </div>
                <div class="clearfix">
                    <table class="clndr-table" border="0" cellspacing="0" cellpadding="0">
                        <thead>
                        <tr class="header-days">
                            <% _.each(daysOfTheWeek, function(day) { %>
                                <td class="header-day"><%= day %></td>
                            <% }); %>
                        </tr>
                        </thead>
                        <tbody>
                        <% for(var i = 0; i < numberOfRows; i++){ %>
                            <tr>
                                <% for(var j = 0; j < 7; j++){ %>
                                    <% var d = j + i * 7; %>
                                    <% if (days[d]) { %>
                                        <td class="<%= days[d].classes %>"
                                            data-toggle="<%= days[d].events[0] ? 'tooltip' : '' %>"
                                            title="<%= days[d].events[0] ? days[d].events[0]['description'] : '' %>">
                                            <div class="day-contents d-flex justify-content-between">
                                                <div>
                                                    <div class="day-number">
                                                        <%= days[d].day < 10 ? '&nbsp;&nbsp;' + days[d].day + '&nbsp;&nbsp;' : '&nbsp;' + days[d].day + '&nbsp;' %>
                                                    </div>
                                                </div>
                                                <div class="emoji">
                                                    <%= days[d].events[0] ? days[d].events[0].emoji : '' %>
                                                </div>
                                            </div>
                                            <% if (days[d].events.length > 0) { %>
                                                <div class="day-weather d-flex pt-2 justify-content-center">
                                                    <div class="temperature">
                                                        <span class="t-max"><%= days[d].events[0].temp_max %></span>
                                                        <span class="t-min text-small text-muted">&nbsp;/&nbsp;<%= days[d].events[0].temp_min %></span>
                                                    </div>
                                                </div>
                                            <% } %>
                                        </td>
                                    <% } else { %>
                                        <td class="day inactive">
                                            <div class="day-contents"></div>
                                        </td>
                                    <% } %>
                                <% } %>
                            </tr>
                        <% } %>
                        </tbody>
                    </table>
                </div>
            </script>

        </div>
        {{-- <img src="{{asset('img/hero-img.png')}}" alt="Hero Imgs">--}}
        {{-- <a href="#get-started" class="btn-get-started scrollto">Get Started</a>--}}

        <div class="row w-100" id="supports">
            <div class="col-md-4 offset-md-4">
                <h2 class="mb-5">Supports</h2>

                <div class="d-flex justify-content-around mt-3">
                    <div>
                        <div>
                            <img src="{{asset('img/ical.png')}}" width="50">
                        </div>
                        <div>iCal</div>
                    </div>
                    <div>
                        <div>
                            <img src="{{asset('img/gcalendar.png')}}" width="50">
                        </div>
                        <div>
                            Google
                        </div>
                    </div>
                    <div>
                        <div class="" style="padding: 3px">
                            <img src="{{asset('img/outlook.jpg')}}" width="40">
                        </div>
                        <div>Outlook</div>
                    </div>

                    <div>
                        <div class="text-right" style="height: 50px; width: 50px; margin-bottom: 7px">
                            <i class="fa fa-ellipsis-h fa-3x text-secondary"></i>
                        </div>
                        <div>And more</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- #hero -->

<!--==========================
  About Us Section
============================-->
<section id="about-us" class="about-us padd-section wow fadeInUp">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 mb-5">
                <div class="about-content">

                    <h1 class="text-center mb-5"><span class="text-success">iCalWeather</span> </h1>
                    <h2 class="text-center mb-3">Get <b class="text-secondary">forecast</b> & enjoy your <b class="text-secondary">planning</b></h2>
                    <p>
                        Simply copy weather calendar URL, subscribe to it by url in your favorite calendar tool and Voi la!
                        You get monthly weather forecast in your calendar. It is useful to plan your travels, trips, outdoor activities and so on.
                    </p>

                </div>
            </div>

            <div class="col-md-8">
                <img style="border: 1px solid #dedede;" src="{{asset('img/screenshot.png')}}" alt="About">
            </div>

        </div>
    </div>
</section>

<!--==========================
  Features Section
============================-->

<section id="features" class="padd-section text-center wow fadeInUp">

    <div class="container">
        <div class="section-title text-center">
            <h2><span class="text-success" style="font-weight: 300">iCalWeather</span> <b class="text-secondary">Features</b></h2>
        </div>
    </div>

    <div class="container">
        <div class="row">

            <div class="col-md-6 col-lg-3">
                <div class="feature-block">
                    <img src="{{asset('img/calendar.png')}}" alt="img" class="img-fluid">
                    <h4>Multiple calendars</h4>
                    <p>You can import weather calendar by URL to any calendar tool that supports subscribing by URL</p>
                </div>
            </div>

            <div class="col-md-6 col-lg-3">
                <div class="feature-block">
                    <img src="{{asset('img/sunny.png')}}" alt="img" class="img-fluid">
                    <h4>Supports emoji</h4>
                    <p>Adds emoji icons to daily weather forecast for better readability and space saving</p>
                </div>
            </div>

            <div class="col-md-6 col-lg-3">
                <div class="feature-block">
                    <img src="{{asset('img/temperature.png')}}" alt="img" class="img-fluid">
                    <h4>Temperature scales</h4>
                    <p>You can choose temperature scale you like. Now C° and F° are supported</p>
                </div>
            </div>

            <div class="col-md-6 col-lg-3">
                <div class="feature-block">
                    <img src="{{asset('img/translate.png')}}" alt="img" class="img-fluid">
                    <h4>Multilingual</h4>
                    <p>You can choose forecast language you like. Now UA and EN are supported</p>
                </div>
            </div>

        </div>
    </div>
</section>

<!--==========================
  Video Section
============================-->

<section class="text-center wow fadeInUp">
    <div class="container">
        <div class="section-title text-center">
            <h2>How subscribe to calendar by URL?</h2>
            <p class="separator">See how it works by gogle calendar example</p>
        </div>
    </div>

    <div id="video">
        <div class="overlay">
            <div class="container-fluid container-full">

                <div class="row">
                    <a href="#" class="js-modal-btn play-btn" data-video-id="RC8H0kqtJoI"></a>
                </div>

            </div>
        </div>
    </div>
</section>


<!--==========================
  Footer
============================-->
<footer class="footer">
<!--    <div class="container">-->
<!--        <div class="row">-->

<!--            <div class="col-md-12 col-lg-4">-->
<!--                <div class="footer-logo">-->

<!--                    <a class="navbar-brand" href="#">eStartup</a>-->
<!--                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been-->
<!--                        the industry's standard dummy text ever since the.</p>-->

<!--                </div>-->
<!--            </div>-->

<!--            <div class="col-sm-6 col-md-3 col-lg-2">-->
<!--                <div class="list-menu">-->

<!--                    <h4>Abou Us</h4>-->

<!--                    <ul class="list-unstyled">-->
<!--                        <li><a href="#">About us</a></li>-->
<!--                        <li><a href="#">Features item</a></li>-->
<!--                        <li><a href="#">Live streaming</a></li>-->
<!--                        <li><a href="#">Privacy Policy</a></li>-->
<!--                    </ul>-->

<!--                </div>-->
<!--            </div>-->

<!--            <div class="col-sm-6 col-md-3 col-lg-2">-->
<!--                <div class="list-menu">-->

<!--                    <h4>Abou Us</h4>-->

<!--                    <ul class="list-unstyled">-->
<!--                        <li><a href="#">About us</a></li>-->
<!--                        <li><a href="#">Features item</a></li>-->
<!--                        <li><a href="#">Live streaming</a></li>-->
<!--                        <li><a href="#">Privacy Policy</a></li>-->
<!--                    </ul>-->

<!--                </div>-->
<!--            </div>-->

<!--            <div class="col-sm-6 col-md-3 col-lg-2">-->
<!--                <div class="list-menu">-->

<!--                    <h4>Support</h4>-->

<!--                    <ul class="list-unstyled">-->
<!--                        <li><a href="#">faq</a></li>-->
<!--                        <li><a href="#">Editor help</a></li>-->
<!--                        <li><a href="#">Contact us</a></li>-->
<!--                        <li><a href="#">Privacy Policy</a></li>-->
<!--                    </ul>-->

<!--                </div>-->
<!--            </div>-->

<!--            <div class="col-sm-6 col-md-3 col-lg-2">-->
<!--                <div class="list-menu">-->

<!--                    <h4>Abou Us</h4>-->

<!--                    <ul class="list-unstyled">-->
<!--                        <li><a href="#">About us</a></li>-->
<!--                        <li><a href="#">Features item</a></li>-->
<!--                        <li><a href="#">Live streaming</a></li>-->
<!--                        <li><a href="#">Privacy Policy</a></li>-->
<!--                    </ul>-->

<!--                </div>-->
<!--            </div>-->

<!--        </div>-->
<!--    </div>-->

    <div class="copyrights">
        <div class="container">
            <p>&copy; Copyrights iCalWeather. All rights reserved.</p>
            <div class="credits">
                <!--
                  All the links in the footer should remain intact.
                  You can delete the links only if you purchased the pro version.
                  Licensing information: https://bootstrapmade.com/license/
                  Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=eStartup
                -->
                Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
            </div>
        </div>
    </div>

</footer>

<!-- Modal -->
<div class="modal fade" id="citiesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Choose your location</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

<!-- JavaScript Libraries -->
<script src="{{asset('lib/jquery/jquery.min.js')}}"></script>
<script src="{{asset('lib/jquery/jquery-migrate.min.js')}}"></script>
<script src="{{asset('lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('lib/superfish/hoverIntent.js')}}"></script>
<script src="{{asset('lib/superfish/superfish.min.js')}}"></script>
<script src="{{asset('lib/easing/easing.min.js')}}"></script>
<script src="{{asset('lib/modal-video/js/modal-video.js')}}"></script>
<script src="{{asset('lib/owlcarousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('lib/wow/wow.min.js')}}"></script>
<script src="{{asset('lib/underscore.js')}}"></script>
<script src="{{asset('lib/moment.js')}}"></script>
<script src="{{asset('lib/clndr.js')}}"></script>
<!-- Contact Form JavaScript File -->
<script src="{{asset('js/contactform.js')}}"></script>

<!-- Template Main Javascript File -->
<script src="{{asset('js/main.js')}}"></script>
<script src="{{asset('js/clndr.js')}}"></script>
<script src="{{asset('js/search.js')}}"></script>

</body>
</html>
