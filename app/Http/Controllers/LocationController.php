<?php

namespace App\Http\Controllers;

use App\Services\CityService;
use Laravel\Lumen\Routing\Controller as BaseController;

class LocationController extends BaseController
{
    /**
     * @param CityService $cityService
     * @param string $query
     * @return array
     */
    public function search(CityService $cityService, string $query)
    {
        return $cityService->search($query);
    }
}
