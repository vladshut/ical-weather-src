<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class MainController extends BaseController
{
    /**
     * @return string
     */
    public function index()
    {
        return view('home');
    }
}
