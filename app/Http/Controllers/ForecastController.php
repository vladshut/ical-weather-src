<?php

namespace App\Http\Controllers;

use App\Services\WeatherService;
use Laravel\Lumen\Routing\Controller as BaseController;

class ForecastController extends BaseController
{
    /**
     * @param WeatherService $weatherService
     * @param string $locationId
     * @return array
     */
    public function index(WeatherService $weatherService, string $locationId)
    {
        $forecast = $weatherService->getForecast($locationId);
        $lang = 'en';
        $tempScale = 'c';
        $iCalUrl = route('Ical.getForecast', compact('locationId', 'lang', 'tempScale'));

        return compact('forecast', 'iCalUrl');
    }
}
