<?php

namespace App\Http\Controllers;

use App\Services\WeatherService;
use Eluceo\iCal\Component\Calendar;
use Eluceo\iCal\Component\Event;
use Laravel\Lumen\Routing\Controller as BaseController;

class IcalController extends BaseController
{
    /**
     * @param WeatherService $weatherService
     * @param string $locationId
     * @param string $lang
     * @param string $tempScale
     * @return void
     */
    public function getForecast(WeatherService $weatherService, string $locationId, string $lang = 'en', string $tempScale = 'c')
    {
        $tempScales = ['c', 'f'];
        $langs = ['en', 'ua'];

        if (!in_array($tempScale, $tempScales)) {
            $tempScale = $tempScales[0];
        }

        if (!in_array($lang, $langs)) {
            $lang = $langs[0];
        }

        $vCalendar = new Calendar('forecast');

        $weatherForecast = $weatherService->getForecast($locationId);

        foreach ($weatherForecast as $item) {
            $tMax = $item['temp_max_' . $tempScale];
            $tMin = $item['temp_min_' . $tempScale];
            $desc = $item['description_' . $lang];
            $emoji = $item['emoji'];

            $summary = "$emoji $tMax ($tMin) $desc";

            $vEvent = new Event();
            $vEvent
                ->setDtStart($item['date'])
                ->setDtEnd($item['date'])
                ->setNoTime(true)
                ->setSummary($summary)
                ->setCategories(['weather'])
                ->setLocation('Kyiv')
                ->setUseTimezone(true);
            ;

            $vCalendar->addComponent($vEvent);
        }

        header('Content-Type: text/calendar; charset=utf-8');
        header('Content-Disposition: attachment; filename="cal.ics"');

        echo $vCalendar->render();
    }
}
