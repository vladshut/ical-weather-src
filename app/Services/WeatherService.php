<?php
/**
 * Created by PhpStorm.
 *
 * Date: 17.03.19
 * Time: 13:21
 */

namespace App\Services;


use DateTime;
use Illuminate\Support\Facades\Cache;

class WeatherService
{
    /**
     * @var ContentFromUrlFetcher
     */
    protected $contentFetcher;

    /**
     * @var HtmlContentParser
     */
    protected $contentParser;
    private $cacheKeyPrefix = 'weather-forecast-by-location-id::';

    /**
     * FetchWeatherCommand constructor.
     * @param ContentFromUrlFetcher $contentFromUrlFetcher
     * @param HtmlContentParser $contentParser
     */
    public function __construct(ContentFromUrlFetcher $contentFromUrlFetcher, HtmlContentParser $contentParser)
    {
        $this->contentFetcher = $contentFromUrlFetcher;
        $this->contentParser = $contentParser;
    }

    /**
     * @param string $locationId
     * @return array
     */
    public function getForecast(string $locationId): array
    {
        $cacheKey = $this->getCacheKey($locationId);

        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        $weatherData = [];

        $url = "https://www.gismeteo.ua/ua/weather-{$locationId}/month/";

//        $content = file_get_contents('response.html');
         $content = $this->contentFetcher->fetchFrom($url);

        if (!$content) {
            return $weatherData;
        }

        $weatherData = $this->contentParser->getForecastFrom($content, $url);

        if (empty($weatherData)) {
            return [];
        }

        $endDate = new DateTime();
        $endDate->setTime(23, 59, 59);

        Cache::put($cacheKey, $weatherData, $endDate);

        return $weatherData;
    }

    private function getCacheKey(string $locationId)
    {
        return $this->cacheKeyPrefix . $locationId;
    }
}