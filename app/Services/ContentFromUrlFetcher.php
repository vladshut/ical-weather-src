<?php
/**
 * Created by PhpStorm.
 *
 * Date: 17.03.19
 * Time: 13:21
 */

namespace App\Services;


use Campo\UserAgent;
use Exception;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

class ContentFromUrlFetcher
{
    /**
     * @param string $url
     * @return string
     * @throws GuzzleException
     */
    public function fetchFrom(string $url): string
    {
        try {
            $guzzleClient = new GuzzleClient([RequestOptions::HEADERS => [
                'User-Agent' => UserAgent::random()
            ]]);

            $response = $guzzleClient->request('get', $url);

            return $response->getBody()->getContents();
        } catch (Exception | GuzzleException $e) {
            throw $e;
        }
    }
}