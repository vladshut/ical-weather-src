<?php
/**
 * Created by PhpStorm.
 *
 * Date: 17.03.19
 * Time: 13:21
 */

namespace App\Services;


use Illuminate\Support\Facades\Cache;

class CityService
{
    private $cacheKeyPrefix = 'city-options-by-query::';

    /**
     * @var ContentFromUrlFetcher
     */
    protected $contentFetcher;

    /**
     * @var HtmlContentParser
     */
    protected $contentParser;

    /**
     * FetchWeatherCommand constructor.
     * @param ContentFromUrlFetcher $contentFromUrlFetcher
     * @param HtmlContentParser $contentParser
     */
    public function __construct(ContentFromUrlFetcher $contentFromUrlFetcher, HtmlContentParser $contentParser)
    {
        $this->contentFetcher = $contentFromUrlFetcher;
        $this->contentParser = $contentParser;
    }

    /**
     * @param string $query
     * @return array
     */
    public function search(string $query): array
    {
        $cacheKey = $this->getCacheKey($query);

        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        $options = [];

        $query = urlencode($query);
        $url = "https://www.gismeteo.ua/ua/search/{$query}/";

//        $content = file_get_contents('response_city.html');
         $content = $this->contentFetcher->fetchFrom($url);

        if (!$content) {
            return $options;
        }

        $cityOptions = $this->contentParser->getCityListFrom($content, $url);

        Cache::forever($cacheKey, $cityOptions);

        return $cityOptions;
    }

    private function getCacheKey(string $query)
    {
        return $this->cacheKeyPrefix . $query;
    }
}