<?php
/**
 * Created by PhpStorm.
 *
 * Date: 23.03.19
 * Time: 11:18
 */

namespace App\Services;


use DateTime;
use Symfony\Component\DomCrawler\Crawler;

class HtmlContentParser
{
    /**
     * @param string $htmlContent
     * @param string|null $url
     * @return array|null
     */
    public function getForecastFrom(string $htmlContent, string $url = null): ?array
    {

        $crawler = new Crawler(null, $url);
        $crawler->addContent($htmlContent, 'text/html');

        if (!$crawler) {
            return null;
        }

        $weatherCells = $crawler->filter('.weather-cells .cell:not(.empty)');

        $weatherData = [];

        $weatherCells->each(function (Crawler $cell, $i) use (&$weatherData) {
            $date = $cell->filter('.date')->first();
            $tempMax = $cell->filter('.temp_max .unit_temperature_c')->first();
            $tempMin = $cell->filter('.temp_min .unit_temperature_c')->first();
            $tempMaxF = $cell->filter('.temp_max .unit_temperature_f')->first();
            $tempMinF = $cell->filter('.temp_min .unit_temperature_f')->first();
            $desc = $cell->attr('data-text');

            $date = $date ? preg_replace("/[^0-9]/", "", $date->text()) : null;
            $tempMax = $tempMax ? trim($tempMax->text()) : null;
            $tempMin = $tempMin ? trim($tempMin->text()) : null;
            $tempMaxF = $tempMaxF ? trim($tempMaxF->text()) : null;
            $tempMinF = $tempMinF ? trim($tempMinF->text()) : null;
            $emoji = $this->getEmojiByDesc((string)$desc);

            $weatherData[] = [
                'day' =>  $date,
                'temp_max_c' => $tempMax,
                'temp_min_c' => $tempMin,
                'temp_max_f' => $tempMaxF,
                'temp_min_f' => $tempMinF,
                'description_en' => $this->getEnDesc($desc ?: ''),
                'description_ua' => $desc,
                'summary' => "$emoji $tempMax ($tempMin) $desc",
                'emoji' => "$emoji",
            ];
        });

        $month = date('n');
        $year = date('Y');
        $day = date('j');
        $monthFlag = -1;

        foreach ($weatherData as &$w) {
            if (!$w['day']) {
                continue;
            }

            $dateDay = $w['day'];
            $dateMonth = $month;
            $dateYear = $year;

            if ($w['day'] > $day && $monthFlag === -1) {
                $dateMonth = date('n', strtotime('-1 month'));
                $dateYear = $dateMonth > $month ? ($year - 1) : $year;
            } else if ($w['day'] === $day && $monthFlag === -1) {
                $monthFlag = 0;
            } else if ($w['day'] < $day && $monthFlag === 0) {
                $dateMonth = date('n', strtotime('+1 month'));
                $dateYear = $dateMonth < $month ? $year + 1 : $year;
                $monthFlag = 1;
            } else if ($monthFlag === 1) {
                $dateMonth = date('n', strtotime('+1 month'));
                $dateYear = $dateMonth < $month ? $year + 1 : $year;
            }

            $w['date'] = DateTime::createFromFormat('Y-n-j', "$dateYear-$dateMonth-$dateDay");
        }

        return $weatherData;
    }

    public function getCityListFrom(string $htmlContent, string $url = null): array
    {
        $crawler = new Crawler(null, $url);
        $crawler->addContent($htmlContent, 'text/html');

        if (!$crawler) {
            return [];
        }

        $optionNodes = $crawler->filter('.catalog_item');

        $options = [];

        $optionNodes->each(function (Crawler $o) use (&$options) {
            $linkNodes = $o->filter('a');
            $forecastLinkNode = $linkNodes->first();
            $linkNodes = $linkNodes->slice(1);

            $name = trim($forecastLinkNode->text());

            $fullNameParts = [];
            $linkNodes->each(function (Crawler $a) use (&$fullNameParts) {
                $fullNameParts[] = $a->text();
            });

            $fullName = implode(', ', $fullNameParts);

            preg_match('/weather-(?<id>.*)\/$/ui', $forecastLinkNode->attr('href'),$matches);
            $id = $matches['id'];

            $options[] = [
                'name' => translit($name),
                'id' => $id,
                'fullName' => translit($fullName),
            ];
        });

        return $options;
    }

    /**
     * @param string $desc
     * @return string
     */
    private function getEmojiByDesc(string $desc): string
    {
        $emojiMap = [
            '🌞' => 'Ясно',
            '⛈' => '(?=.*\дощ\b)(?=.*\гроза\b).*',
            '☁️'  => 'Похмуро',
            '🌤' => 'Слабка хмарність',
            '🌧' => '(?=.*\похмуро\b)(?=.*\дощ\b).*',
            '🌦' => '(?=.*\дощ\b)(?=.*?(хмарно|хмарність)).*',
            '🌨' => '(?=.*\сніг\b).*',
            '⛅' => '(?=.*?(хмарно|хмарність|похмуро)).*',
        ];

        foreach ($emojiMap as $emoji => $regex) {
            if (preg_match("/^{$regex}$/mui", $desc)) {
                return $emoji;
            }
        }

        return '';
    }

    /**
     * @param string $desc
     * @return string
     */
    private function getEnDesc(string $desc): string
    {
        $descMap = [
            'Sunny' => 'Ясно',
            'Rain with a thunderstorm' => '(?=.*\дощ\b)(?=.*\гроза\b).*',
            'Cloudy️'  => 'Похмуро',
            'Cloudy, rain' => '(?=.*\похмуро\b)(?=.*\дощ\b).*',
            'Partly cloudy, rain' => '(?=.*\дощ\b)(?=.*?(хмарно|хмарність)).*',
            'Snow' => '(?=.*\сніг\b).*',
            'Partly cloudy' => '(?=.*?(хмарно|хмарність|похмуро)).*',
        ];

        foreach ($descMap as $en => $regex) {
            if (preg_match("/^{$regex}$/mui", $desc)) {
                return $en;
            }
        }

        return translit($desc);
    }
}