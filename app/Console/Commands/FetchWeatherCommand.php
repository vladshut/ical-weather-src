<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;


use App\Services\WeatherService;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;


/**
 * Class deletePostsCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class FetchWeatherCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "app:fetch-weather";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Fetch weather forecast.";

    /** @var WeatherService */
    protected $weatherService;

    /**
     * FetchWeatherCommand constructor.
     * @param WeatherService $cityService
     */
    public function __construct(WeatherService $cityService)
    {
        parent::__construct();
        $this->weatherService = $cityService;
    }

    /**
     * Execute the console command.
     *
     * @throws GuzzleException
     */
    public function handle()
    {
        try {
            $weatherData = $this->weatherService->getForecast('kyiv-4944');

            if (empty($weatherData)) {
                $this->warn('Empty weather data!');
            }

            var_dump($weatherData);

            $this->info('Finish!');
        } catch (Exception $e) {
            dump($e->getMessage(), $e->getTraceAsString());
            $this->error("An error occurred");
        }
    }
}
