<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;


use App\Services\CityService;
use App\Services\WeatherService;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;


/**
 * Class deletePostsCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class SearchCityCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "app:search-city";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Search city by query.";

    /** @var CityService */
    protected $cityService;

    /**
     * FetchWeatherCommand constructor.
     * @param CityService $cityService
     */
    public function __construct(CityService $cityService)
    {
        parent::__construct();
        $this->cityService = $cityService;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        try {
            $cityOptions = $this->cityService->search('Kyiv');

            if (empty($cityOptions)) {
                $this->warn('Empty city options!');
            }

            var_dump($cityOptions);

            $this->info('Finish!');
        } catch (Exception $e) {
            dump($e->getMessage(), $e->getTraceAsString());
            $this->error("An error occurred");
        }
    }
}
