<?php

use Laravel\Lumen\Routing\UrlGenerator;

if (!function_exists('urlGenerator')) {
    /**
     * @return UrlGenerator
     */
    function urlGenerator() {
        return new UrlGenerator(app());
    }
}

if (!function_exists('asset')) {
    /**
     * @param $path
     * @param bool $secured
     *
     * @return string
     */
    function asset($path, $secured = true) {
        return urlGenerator()->asset($path, $secured);
    }
}

if (!function_exists('translit')) {
    function translit(string $text) {
        $cyr = [
            'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
            'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
            'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я',
            'і', 'І', 'ї', 'Ї', 'ґ', 'Ґ', 'є', 'Є',
        ];
        $lat = [
            'a','b','v','g','d','e','io','zh','z','u','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','\'','e','yu','ya',
            'A','B','V','G','D','E','Io','Zh','Z','I','Y','K','L','M','N','O','P',
            'R','S','T','U','F','H','Ts','Ch','Sh','Sht','A','I','Y','e','Yu','Ya',
            'i', 'I', 'yi', 'Yi', 'g', 'G', 'ye', 'Ye',
        ];

        return str_replace($cyr, $lat, $text);
    }
}
