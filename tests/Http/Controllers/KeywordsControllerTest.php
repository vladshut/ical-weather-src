<?php

use App\Services\UrlKeywordsGenerator;

/**
 * Created by PhpStorm.
 *
 * Date: 17.03.19
 * Time: 12:38
 */

class KeywordsControllerTest extends TestCase
{
    /** @var \Mockery\MockInterface */
    private $keywordGeneratorMock;

    private $keyword = 'Google';

    public function setUp(): void
    {
        parent::setUp();
        $this->setUpMocks();
    }

    /**
     *
     */
    public function testGenerate()
    {
        $url = 'https://google.com.ua';
        $this->post('/generate-keywords', compact('url'));
        $this->seeStatusCode(200);
        $this->seeJson([$this->keyword]);
    }

    /**
     *
     */
    private function setUpMocks()
    {
        $this->keywordGeneratorMock = $this->mock(UrlKeywordsGenerator::class)
            ->shouldReceive('generate')
            ->andReturn([$this->keyword])
        ;
    }
}