<?php

use App\Services\HtmlContentParser;

/**
 * Created by PhpStorm.
 *
 * Date: 19.03.19
 * Time: 19:58
 */

class HtmlContentParserTest extends TestCase
{
    /**
     * @dataProvider htmlFilesProvider
     * @param $fileName
     * @param $expectedTitle
     */
    public function testGetTitleFrom($fileName, $expectedTitle)
    {
        $converter = new HtmlContentParser();

        $filePath = __DIR__ . "/Fixtures/$fileName";
        $htmlContent = file_get_contents($filePath);

        $title = $converter->getTitleFrom($htmlContent);

        $this->assertEquals($expectedTitle, $title);
    }

    public function htmlFilesProvider()
    {
        return [
            [
                'html_content_01.html',
                'Новини з окупованного Криму – читайте та дивіться на Громадському',
            ],
            [
                'html_content_02.html',
                'Service Container - Laravel - The PHP Framework For Web Artisans',
            ],
        ];
    }
}